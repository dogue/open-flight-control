/// Toggleable modes for the autopilot.
pub enum Modes {
    LateralNavigation   = 0,
    HeadingHold         = 1,
    AltitudeHold        = 2,
    VerticalSpeedHold   = 3,
    SpeedUnitsMach      = 4,
    AutopilotMaster     = 5,
    YawDamper           = 6,
    FlightLevelChange   = 7,
    VerticalNavigation  = 8,
    AirspeedHold        = 9,
    Autothrottle        = 10,
    Backcourse          = 11
}

pub struct Autopilot {
    
}