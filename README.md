# Open Flight Control

Open Flight Control is a (WIP) framework for developing hardware controllers for Microsoft Flight Simulator 2020.

## Installation

TODO

## Usage

TODO

## Docs

The code is being documented as it is written. The docs [found here](https://dogue.gitlab.io/open-flight-control) will always be current to the main branch.

## License
[MIT](https://choosealicense.com/licenses/mit/)